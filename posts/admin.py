from django.contrib import admin
from .models import Post

# Register your models here.
# Superuser:admin Password:MyBlog7654321

admin.site.register(Post)
